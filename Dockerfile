FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /opt/app

ARG JAR_FILE=target/gateway-1.0.jar
COPY ${JAR_FILE} gateway-1.0.jar

ENTRYPOINT ["java","-jar","gateway-1.0.jar"]

