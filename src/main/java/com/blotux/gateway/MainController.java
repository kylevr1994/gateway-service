package com.blotux.gateway;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.http.HttpHeaders;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MainController {

    @RequestMapping(value = "/test")
    public String test(){
        return "The service has started correctly";
    }

    /**
     * Redirect the JSON data to the right service.
     * @return
     */
    @RequestMapping(value = "/app")
    public String app(@RequestBody Map<String,Object> payload, @RequestHeader MultiValueMap<String,String> headers, @RequestHeader(value = "Authorization") String Auth) {

        try{
            // Connect to the authentication service and check if the user has the correct accessToken
            if(!Auth.isEmpty()){
                final String uri = "http://localhost:8081/token_check";
                RestTemplate restTemplate = new RestTemplate();
                HttpEntity<?> httpEntity = new HttpEntity<Object>(payload, headers);
                ResponseEntity result = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, Boolean.class);
                if(!result.getBody().equals(true)) { return "no access"; }
            }
        } catch(HttpClientErrorException.BadRequest exception){
            return "bad request";
        }

        // Send the request to the right service.
        return "has access";

    }

    /**
     * Gateway to the authentication service to check if the login credentials are correct
     * @param payload email and password
     * @return service sessionKey if credentials are correct, null not
     */
    @RequestMapping(value = "/login")
    public ResponseEntity login(@RequestBody Map<String, Object> payload, @RequestHeader MultiValueMap<String,String> headers) {
        try{
            final String uri = "http://localhost:8081/login";
            RestTemplate restTemplate = new RestTemplate();
            HttpEntity<?> httpEntity = new HttpEntity<Object>(payload, headers);
            ResponseEntity result = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, Object.class);
            return result;

        } catch (HttpClientErrorException.BadRequest exception){
            return null;
        }
    }

    @RequestMapping(value = "/register")
    public ResponseEntity register(@RequestBody Map<String, Object> payload, @RequestHeader MultiValueMap<String,String> headers) {
        try{
            final String uri = "http://localhost:8081/register";
            RestTemplate restTemplate = new RestTemplate();
            HttpEntity<?> httpEntity = new HttpEntity<Object>(payload, headers);
            ResponseEntity result = restTemplate.exchange(uri, HttpMethod.POST, httpEntity, Object.class);
            return result;

        } catch (HttpClientErrorException.BadRequest exception){
            return null;
        }
    }

    /**
     * Gateway to the authentication service to check if the session credentials are correct
     * @return service sessionKey if credentials are correct, null not
     */
    @RequestMapping(value = "/access")
    public ResponseEntity accessCheck(@RequestBody Map<String,Object> payload, @RequestHeader MultiValueMap<String,String> headers, @RequestHeader(value = "Authorization") String Auth) {

        try{
            // Connect to the authentication service and check if the user has the correct accessToken
            if(!Auth.isEmpty()){
                final String uri = "http://localhost:8081/token_check";
                RestTemplate restTemplate = new RestTemplate();
                HttpEntity<?> httpEntity = new HttpEntity<Object>(payload, headers);
                ResponseEntity result = restTemplate.exchange(uri, HttpMethod.GET, httpEntity, String.class);
                return result;
            }

            return null;
        } catch(HttpClientErrorException.BadRequest exception){
            return null;
        }
    }
}
