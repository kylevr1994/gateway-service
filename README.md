# Gateway service

This microservice is used to route requests to the right service

<b>Build executable file command:</b>
mvn clean package <br>
<b> Run command: </b> mvn spring-boot:run <br>
<b> Build Docker image: </b> docker build -t perfectposter/gateway:latest . <br>
<b> Run Docker image: </b>docker run -d -p 8080:8080 perfectposter/gateway:latest
